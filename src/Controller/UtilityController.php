<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 04/06/2018
 * Time: 23:43
 */
class UtilityController extends Controller
{
    /**
     * @Route("/{_locale}/contact", name="contact", defaults={"_locale"="en"}, requirements={
     *     "_locale"="en|fr"
     * })
     */
    public function index(Request $request)
    {

        $name = $request->get("name");
        $email = $request->get("email");
        $subject = $request->get("subject");
        $messageForm = $request->get("message");

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo('medchamsibardi@gmail.com')
            ->setBody(
                $this->renderView(
                    "emails/contact.html.twig",
                    array(
                        'name' => $name,
                        'email' => $email,
                        'subject' => $subject,
                        'message' => $messageForm,
                    )
                ),
                'text/html'
            );

        $this->get('mailer')->send($message);


        return new Response("");
    }

}