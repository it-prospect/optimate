## Installation Docker:
Ajouter la clé GPG du repository officiel à Docker

```shell 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

Ajouter le repository au fichier source de apt

```shell 
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```

Mise a jour, pour referencer les entrées ajouté

```shell 
sudo apt-get update
```

Enforcer que docker est installé depuis les nouvelles sources et non celle existante sous Ubuntu par defaut

```shell 
apt-cache policy docker-ce
```

Finalement, on install le packet

```shell 
sudo apt-get install docker-ce docker-compose
```

Verifier que docker est fonctionnel

```shell 
sudo systemctl status docker
```
PS: Pour les abitués de "service <svc> status", la commande fonctionne mais c'est deprécié

## Ajout l'accés à la commande docker pour les utilisateurs non Root
Maintenant que Docker est installé, il faut que l'on accorde aux utilisateurs non privilégiés le droit de l'utiliser, pour cela nous devrons d'abord ajouter l'utilisateur en question au groupe docker

```shell 
sudo usermod -aG docker ${USER}
```
PS: Si nous ne sommes pas connectés directement avec l'utilisateur en question, il est impératif de remplacer ${USER} avec l'utilisateur que nous souhaitons ajouter

ceci fait, pour que la modification soit fonctionnelle, nous devions faire nous logger à nouveau au compte utilisateur en question 

```shell 
su - ${USER}
```

Pour vérifier que notre utilisateur fait bien partie du groupe docker, nous exécutons la commande suivante

```shell 
id -nG
```

le résultat de cette commande doit afficher un resultat comme celui-ci

```shell 
user sudo docker
```

## Installation du projet
Afin de faire fonctionner le projet, nous devions d'abord le cloner de git, supposons que nous allons tout installer sous /var/www, les commandes que nous aurons à éxecuter serons les suivantes:

```shell 
cd /var/www
git clone git@bitbucket.org:it-prospect/optimate.git
```
PS: Il est préférable de mettre une clé ssh pour l'authentification de GIT afin de ne pas avoir à retaper les identifiants à chaque commande

Ceci fait, nous nous plaçons dans le projet en question et initialisons les variables de deploiment à la main(Vu que Jenkins n'a pas encore été configuré)

```shell 
cp .env.dist .env
vi .env
```

Changer dans .env les parametres d'environement

Entre autre si nous travaillons en local, il faudra rajouter une entrée dans `/etc/hosts` comme suit:

```shell
sudo -s
echo '127.0.0.1 www.optimate.local' >> /etc/hosts
```

Finalement nous déployons le projet tout simplement avec la suite de commandes qui suit:

```shell 
cd /var/www/optimate
docker-compose up -d
```

Vérifier que les conteneurs crossover_php, crossover_ngnix et crossover_mysql sont bien montés:

```shell
docker ps
```

Pour accéder au site : `www.optimate.local:88`
Pour accéder à la base de données: 

host : `localhost`

port : `3312`

Username: celui dans le fichier `.env`

password: celui dans le fichier `.env`

Pour accéder au theme : `http://www.optimate.local:88/theme/index.html`